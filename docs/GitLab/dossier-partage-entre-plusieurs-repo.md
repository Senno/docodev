---
sidebar_position: 1
---

# Créer un dossier partagé entre plusieurs depots GitLab !

Mon objectif : Avoir un répot avec un dossier .gitlab ou je place tous mes templates d'issue et de merge request.

Je commence par créé mon dépot de dossier partagé, j'y place un .gitignore, un dossier .gitlab et une pipeline (.gitlab-ci.yml).

```yaml title: Le contenue de la pipeline
image: ubuntu:latest

stages:
  - deploy-docodev
  - deploy-wikibo

before_script:
  - echo "Installation de Git et paramétrage"
  - apt update; apt install -y git; git config --global user.email "romain@ecobosto.fr"; git config --global user.name "Senno"
  - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
  - mkdir -p ~/.ssh; chmod 700 ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config'

merge-template-docodev:
  stage: deploy-docodev
  script:
    - printf "\n================================================\nDéploiement des templates sur Docodev\n================================================\n\n"
    - printf "=================================================\nPréparation du dépot de templates\n====================================================\n"
    - rm .gitlab-ci.yml; rm .gitignore; rm -rf .git; mkdir wait; mv .gitlab ./wait/; git init --initial-branch=main
    - printf "=================================================\nRécupération du dépot à mettre à jour\n====================================================\n"
    - git pull git@gitlab.com:Senno/docodev.git main --allow-unrelated-histories --force --rebase
    - printf "=================================================\nRéalisation du merge des templates sur le dépot\n====================================================\n"
    - rm -rf .gitlab; mv ./wait/.gitlab .; rm -rf ./wait; git add .; git commit -m "✨ Modification des templates" --allow-empty
    - echo "Push des modifications sur le git de Docodev"
    - printf "=================================================\nPush des modifications sur le git de Docodev\n====================================================\n"
    - git push git@gitlab.com:Senno/docodev.git main:main --force
    - printf "=================================================\nFin de la maj des templates pour Docodev\n====================================================\n"
  only:
    - main
```

Dans le before script, je commence par installer git, puis je paramètre mon SSH

Pour le SSH, je génère une paire de clé, je stock la clé privée dans le repo partagé dans settings > Ci/CD > variables > SSH_PRIVATE_KEY

Pour la clé publique, je viens la stocker sur les repos que je souhaite lier au repo partagé dans settings > repository > deploy keys (je l'appelle BOSTEMPLATE_DEPLOY_KEY)

:::warning
Je fais bien attention à cliquer sur `grant write permissions to this key`
:::

Ensuite, il ne reste plus qu'à ajouter un nouveau `stage` pour chaque dépot que je vais lier (ici deploy-docodev). Il faudra bien sur y mettre tous les scripts qui vont bien

Si jamais il y a des restrictions de push sur la branche main, on va aller dans settings > repository > protected branches > main et dans le **allowed to push** on va venir ajouter notre token qu'on a utilisé avant (BOSTEMPLATE_DEPLOY_KEY)