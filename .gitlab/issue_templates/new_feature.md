# ✨ Le but de la feature

Indiquez à quoi va servir cette nouvelle feature !

# ✏️ Votre solution

Ce que vous proposez pour réaliser la feature

# 🚧 Les issues à terminer avant

1. #issue_number
2. #issue_number_2

/relate #issue_number

/relate #issue_number_2

# 📎 Liens intéressants

- [Site officiel d'Ecobosto](https://www.ecobosto.fr)

/label ~"✨ Feature "